package com.jenius.buyar.detail;

/**
 * Created by kaddafi on 12/08/2017.
 */

public class User {

    private String id;
    private int saldo;

    public User(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
}
