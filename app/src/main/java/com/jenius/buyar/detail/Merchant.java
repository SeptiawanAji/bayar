package com.jenius.buyar.detail;

import android.location.Location;

/**
 * Created by kaddafi on 11/08/2017.
 */

public class Merchant {
    private String namaMerchant;
    private String alamatMerchant;
    private Location location;
    private String pathFoto;
    private String panorama;
    private int idMerchant;

    public String getNamaMerchant() {
        return namaMerchant;
    }

    public void setNamaMerchant(String namaMerchant) {
        this.namaMerchant = namaMerchant;
    }

    public String getAlamatMerchant() {
        return alamatMerchant;
    }

    public void setAlamatMerchant(String alamatMerchant) {
        this.alamatMerchant = alamatMerchant;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getPathFoto() {
        return pathFoto;
    }

    public void setPathFoto(String pathFoto) {
        this.pathFoto = pathFoto;
    }

    public int getIdMerchant() {
        return idMerchant;
    }

    public void setIdMerchant(int idMerchant) {
        this.idMerchant = idMerchant;
    }

    public String getPanorama() {
        return panorama;
    }

    public void setPanorama(String panorama) {
        this.panorama = panorama;
    }
}
