package com.jenius.buyar.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jenius.buyar.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button detailMerchant = (Button) findViewById(R.id.btnDtlMerchantl);
        detailMerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DetailMerchantActivity.class);
                intent.putExtra("idUser","1");
                intent.putExtra("idMerchant","1");
                intent.putExtra("namaMerchant","Cafe");
                intent.putExtra("urlImgMerchant","https://img.monocle.com/stores/cafetokyo-2-50b608b4dbe0b.jpg?h=630&resize=aspectfit&g=center&q=50");
                startActivity(intent);
            }
        });
    }
}
