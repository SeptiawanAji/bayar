package com.jenius.buyar.detail;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jenius.buyar.PanoramaStreetView;
import com.jenius.buyar.R;
import com.jenius.buyar.detail.ListMenuAdapter;
import com.jenius.buyar.detail.MenuMerchant;
import com.jenius.buyar.detail.VAR;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailMerchantActivity extends AppCompatActivity implements View.OnClickListener, AppBarLayout.OnOffsetChangedListener{

    private TextView namaMerchant, alamatMerchant, sliderText, totalBiaya, anotasiJumlahItem, totalBayar, saldoAnda, sisaSaldo, konfirmasiPesanan, peringatanSaldo, results;
    private RecyclerView recyclerView, recyclerViewSelected;
    private ImageView searchButton, clearButton, arrowBack, panorama;
    private NetworkImageView fotoMerchant;
    private EditText inputSearch;
    private Spinner spinnerLayanan, spinnerJmlOrg;

    private List<String> layanan, jmlorg;

    private RelativeLayout sliderFab, sliderHolder;
    private LinearLayout holderPerintah, holderSearch, slider, holderDineIn;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout appBarLayout;
    private SlidingUpPanelLayout slidingUpPanelLayout;

    private Toolbar toolbar;
    private ProgressDialog progressDialog;

    private ListMenuAdapter mAdapter;
    private Merchant merchant;

    private boolean isShow = false, ismenuShow = false;
    private int scrollRange = -1, saldo = 100000, takeaway = 1, jmldinein = 0;
    private String idMerchant, idUser, urlImage, jsonPesanan;

    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_merchant);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        namaMerchant = (TextView) findViewById(R.id.nama_merchant);
        sliderText = (TextView) findViewById(R.id.slider_text);
        totalBiaya = (TextView) findViewById(R.id.total_biaya);
        results = (TextView) findViewById(R.id.result);
        totalBayar = (TextView) findViewById(R.id.pengeluaran);
        saldoAnda = (TextView) findViewById(R.id.saldo);
        sisaSaldo = (TextView) findViewById(R.id.sisa_saldo);
        konfirmasiPesanan = (TextView) findViewById(R.id.konfirmasi_pesanan);
        peringatanSaldo = (TextView) findViewById(R.id.peringatan_saldo);
        anotasiJumlahItem = (TextView) findViewById(R.id.jumlah_item);

        inputSearch = (EditText) findViewById(R.id.input_search);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        recyclerViewSelected = (RecyclerView) findViewById(R.id.recycle_view_selected);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);

        searchButton = (ImageView) findViewById(R.id.search_button);
        clearButton = (ImageView) findViewById(R.id.clear_button);
        arrowBack = (ImageView) findViewById(R.id.arrow_back);
        panorama = (ImageView)findViewById(R.id.panorama);
        fotoMerchant = (NetworkImageView) findViewById(R.id.foto_merchant);

        spinnerJmlOrg = (Spinner) findViewById(R.id.spinner_jml_org);
        spinnerLayanan = (Spinner) findViewById(R.id.spinner_layanan);

        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        holderPerintah = (LinearLayout) findViewById(R.id.holder_perintah);
        holderSearch = (LinearLayout) findViewById(R.id.holder_search);
        slider = (LinearLayout) findViewById(R.id.slider);
        holderDineIn = (LinearLayout) findViewById(R.id.holder_dinein);
        sliderHolder = (RelativeLayout) findViewById(R.id.slider_holder);
        sliderFab = (RelativeLayout) findViewById(R.id.slider_fab);

        imageLoader = CustomVolleyRequest.getInstance(this).getImageLoader();
        getDetailMerchant();
        getData();

        slider.setOnClickListener(this);
        slidingUpPanelLayout.setTouchEnabled(false);
        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if(newState == SlidingUpPanelLayout.PanelState.COLLAPSED){
                    slider.setBackgroundColor(ContextCompat.getColor(DetailMerchantActivity.this, android.R.color.white));
                    sliderFab.setVisibility(View.VISIBLE);
                    sliderHolder.setVisibility(View.VISIBLE);
                    sliderText.setVisibility(View.GONE);
                    arrowBack.setVisibility(View.GONE);
                }else if(newState == SlidingUpPanelLayout.PanelState.EXPANDED){
                    slider.setBackgroundColor(ContextCompat.getColor(DetailMerchantActivity.this, R.color.grey900));
                    sliderFab.setVisibility(View.GONE);
                    sliderHolder.setVisibility(View.GONE);
                    sliderText.setVisibility(View.VISIBLE);
                    sliderText.setTextColor(ContextCompat.getColor(DetailMerchantActivity.this, android.R.color.white));
                    arrowBack.setVisibility(View.VISIBLE);
                }
            }
        });

        slidingUpPanelLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        inputSearch.setOnClickListener(this);
        searchButton.setOnClickListener(this);
        clearButton.setOnClickListener(this);
        panorama.setOnClickListener(this);

        //show toolbar title ONLY when collapse
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle(" ");
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(this);

        slidingUpPanelLayout.setShadowHeight(0);
        slidingUpPanelLayout.setPanelHeight(0);

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mAdapter.getFilter().filter(s.toString());
            }
        });

        final ArrayAdapter<String> adapterLayanan = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, layanan);
        spinnerLayanan.setAdapter(adapterLayanan);
        spinnerLayanan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==0){
                    holderDineIn.setVisibility(View.GONE);
                    jmldinein = 0;
                    takeaway = 1;
                } else if(i==1){
                    holderDineIn.setVisibility(View.VISIBLE);
                    jmldinein = 1;
                    takeaway = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        final ArrayAdapter<String> adapterDineIn = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, jmlorg);
        spinnerJmlOrg.setAdapter(adapterDineIn);
        spinnerJmlOrg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (takeaway==0){
                    jmldinein = (i+1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }

    private void getDetailMerchant() {
        idUser = getIntent().getStringExtra("idUser");
        idMerchant = getIntent().getStringExtra("idMerchant");
        fotoMerchant.setImageUrl(getIntent().getStringExtra("urlImgMerchant"), imageLoader);
        namaMerchant.setText(getIntent().getStringExtra("namaMerchant"));

        layanan = new ArrayList<>();
        layanan.add("Take-away");
        layanan.add("Dine-in");

        jmlorg = new ArrayList<>();
        jmlorg.add("1");
        jmlorg.add("2");
        jmlorg.add("3");
        jmlorg.add("4");

    }

    public void onItemSelected(){
        if(hitungTotalItem(mAdapter.getMenuSelected())!=0){
            slidingUpPanelLayout.setShadowHeight(16);
            slidingUpPanelLayout.setPanelHeight(164);
            int totalbiaya = hitungTotalBiaya(mAdapter.getMenuSelected());
            saldoAnda.setText("Rp "+String.format("%,d", saldo).replace(",","."));
            sisaSaldo.setText("Rp "+String.format("%,d", (saldo-totalbiaya)).replace(",","."));
            if ((saldo-totalbiaya)<0){
                konfirmasiPesanan.setBackgroundColor(ContextCompat.getColor(DetailMerchantActivity.this, R.color.black10));
                konfirmasiPesanan.setClickable(false);
                peringatanSaldo.setVisibility(View.VISIBLE);
            } else {
                konfirmasiPesanan.setBackgroundColor(ContextCompat.getColor(DetailMerchantActivity.this, R.color.colorPrimary));
                konfirmasiPesanan.setClickable(true);
                konfirmasiPesanan.setOnClickListener(this);
                peringatanSaldo.setVisibility(View.GONE);
            }
            totalBiaya.setText("Rp "+String.format("%,d", totalbiaya).replace(",","."));
            totalBayar.setText("Rp "+String.format("%,d", totalbiaya).replace(",","."));
            anotasiJumlahItem.setText(""+hitungTotalItem(mAdapter.getMenuSelected()));
            getJsonPesanan(setNoSelected(mAdapter.getMenuSelected()));
            recyclerViewSelected.setAdapter(new ListMenuKonfirmasiAdapter(setNoSelected(mAdapter.getMenuSelected()), this));
        }else {
            slidingUpPanelLayout.setShadowHeight(0);
            slidingUpPanelLayout.setPanelHeight(0);
            recyclerViewSelected.setAdapter(new ListMenuKonfirmasiAdapter(new ArrayList<MenuMerchant>(),this));
        }
    }

    private void getJsonPesanan(ArrayList<MenuMerchant> menuMerchants){
        try{
            JSONArray ja = new JSONArray();
            for (MenuMerchant menuMerchant:menuMerchants){
                JSONObject jo = new JSONObject();
                jo.put("id", menuMerchant.getId());
                jo.put("jumlah", menuMerchant.getJumlahpesan());
                jo.put("note", menuMerchant.getCatatan());
                ja.put(jo);
            }
            jsonPesanan = "{\"json\":"+ja.toString()+"}";
        } catch (Exception e){
            Log.d(VAR.DEBUG, "error upload bs " +e);
            Toast.makeText(DetailMerchantActivity.this, "Gagal pesan : " +e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private int hitungTotalItem(ArrayList<MenuMerchant> menuMerchants){
        int totalitem = 0;
        for (MenuMerchant menu:menuMerchants) {
            totalitem = totalitem + menu.getJumlahpesan();
        }
        return totalitem;
    }

    private int hitungTotalBiaya(ArrayList<MenuMerchant> menuMerchants){
        int totalbiaya = 0;
        for (MenuMerchant menu:menuMerchants) {
            totalbiaya = totalbiaya + (menu.getHarga() * menu.getJumlahpesan());
        }
        return totalbiaya;
    }

    private ArrayList<MenuMerchant> setNoSelected(ArrayList<MenuMerchant> menuMerchants){
        ArrayList<MenuMerchant> menuSelected = new ArrayList<>();
        for (MenuMerchant menu:menuMerchants) {
            if (menu.getJumlahpesan()>0){
                menuSelected.add(new MenuMerchant(menu.getId(), menu.getNama(), menu.getHarga(), menu.getKategori(), menu.getUrlimg(), menu.getJumlahpesan(), ""));
            }
        }
        return menuSelected;
    }

    public void onResultPublished(int count, String s){
        if(count==0){
            results.setVisibility(View.VISIBLE);
            results.setText("Tidak ditemukan menu '"+s.toString()+"'");
        }else results.setVisibility(View.GONE);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (scrollRange == -1) {
            scrollRange = appBarLayout.getTotalScrollRange();
        }
        if (scrollRange + verticalOffset == 0) {
            collapsingToolbarLayout.setTitle("Pilih Menu");
            toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            ismenuShow = true;
            if(!isShow) invalidateOptionsMenu();
            isShow = true;
        } else if(isShow) {
            collapsingToolbarLayout.setTitle(" ");
            toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
            ismenuShow = false;
            invalidateOptionsMenu();
            isShow = false;
        }
    }

    @Override
    public void onClick(View v) {
        if (v == slider){
            if (slidingUpPanelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.COLLAPSED)){
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            } else {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        }else if(v==inputSearch){
            appBarLayout.setExpanded(false);
        }else if(v==searchButton){
            holderPerintah.setVisibility(View.GONE);
            holderSearch.setVisibility(View.VISIBLE);
            inputSearch.setFocusableInTouchMode(true);
            inputSearch.requestFocus();
            appBarLayout.setExpanded(false);
            showKeyboard();
            ismenuShow = false;
            invalidateOptionsMenu();
        }else if(v==clearButton){
            if(inputSearch.getText().toString().length()>0){
                inputSearch.setText("");
            }else {
                holderPerintah.setVisibility(View.VISIBLE);
                holderSearch.setVisibility(View.GONE);
                appBarLayout.setExpanded(true);
                hideKeyboard();
            }
        }else if (v==konfirmasiPesanan){
            kirimPemesanan();
        }else if(v==panorama){
            Intent intent = new Intent(getApplicationContext(), PanoramaStreetView.class);
            intent.putExtra("panorama",getIntent().getStringExtra("urlPanorama"));
            startActivity(intent);
        }
    }

    private void kirimPemesanan() {
        showProgres();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest req = new StringRequest(Request.Method.POST, VAR.URLORDER, new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(VAR.DEBUG,"konfirmasi pesanan response : "+response);
                    if (jsonObject.getString("status").equals("success")){
                        Intent intent = new Intent(DetailMerchantActivity.this, WaitingConfirmationActivity.class);
                        intent.putExtra("namaMerchant",getIntent().getStringExtra("namaMerchant"));
                        intent.putExtra("idOrder",jsonObject.getString("id_order"));
                        finish();
                        startActivity(intent);
                    } else {
                        Toast.makeText(DetailMerchantActivity.this,"Gagal membuat pesanan",Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(DetailMerchantActivity.this,"Gagal membuat pesanan : " +e.toString(),Toast.LENGTH_SHORT).show();
                    Log.d(VAR.DEBUG, "error konfirmasi pesan : " +e.toString());
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailMerchantActivity.this,"Gagal membuat pesanan : " +error.toString(),Toast.LENGTH_SHORT).show();
                Log.d(VAR.DEBUG, "error konfirmasi pesan : " +error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<String, String>();
                param.put("id_user", idUser);
                param.put("id_merchant", idMerchant);
                param.put("data", jsonPesanan);
                param.put("take_away", ""+takeaway);
                param.put("jml_dinein", ""+jmldinein);
                Log.d(VAR.DEBUG,"json kirim : " +param.toString());
                return param;
            }
        };
        requestQueue.add(req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.search_menu:
                holderPerintah.setVisibility(View.GONE);
                holderSearch.setVisibility(View.VISIBLE);
                inputSearch.setFocusableInTouchMode(true);
                inputSearch.requestFocus();
                appBarLayout.setExpanded(false);
                showKeyboard();
                ismenuShow = false;
                invalidateOptionsMenu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_select_menumerchant, menu);

        if(ismenuShow){
            menu.findItem(R.id.search_menu).setVisible(true);
        }else menu.findItem(R.id.search_menu).setVisible(false);

        Log.d("DEBUG-PAYAR","state "+ismenuShow);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if(slidingUpPanelLayout.getPanelState()==SlidingUpPanelLayout.PanelState.EXPANDED){
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }else super.onBackPressed();
    }

    private void showKeyboard(){
        final InputMethodManager inputMethodManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(inputSearch, InputMethodManager.SHOW_IMPLICIT);

    }

    private void hideKeyboard(){
        InputMethodManager inputManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                this.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void showProgres() {
        progressDialog = null;// Initialize to null
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Harap tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void getData(){
        showProgres();
        merchant = new Merchant();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest getData = new StringRequest(Request.Method.POST, VAR.URLDETAIL, new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                try {
                    ArrayList<MenuMerchant> menuMerchants = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(VAR.DEBUG,"response getdata : "+response);
                    if (jsonObject.getString("status").equals("success")){
                        JSONArray jaMenu = jsonObject.getJSONArray("menu");
                        for (int i=0; i<jaMenu.length(); i++){
                            JSONObject joMenu = jaMenu.getJSONObject(i);
                            MenuMerchant menuMerchant = new MenuMerchant();
                            menuMerchant.setId(joMenu.getString("id_menu"));
                            menuMerchant.setNama(joMenu.getString("nama_menu"));
                            menuMerchant.setKategori(joMenu.getString("kategori"));
                            menuMerchant.setUrlimg(joMenu.getString("path_foto"));
                            menuMerchant.setHarga(joMenu.getInt("harga"));
                            menuMerchants.add(menuMerchant);
                        }
                        setRecycler(menuMerchants);
                        JSONArray jaU = jsonObject.getJSONArray("user_info");
                        JSONObject joU = jaU.getJSONObject(0);
                        saldo = joU.getInt("balance");
                    } else if (jsonObject.getString("status").equals("gagal")){
                        Toast.makeText(DetailMerchantActivity.this,"Gagal memproses",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(VAR.DEBUG, "error getdata : " +e.toString());
                    Toast.makeText(DetailMerchantActivity.this,"Gagal memproses",Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(VAR.DEBUG, "error getdata : " +error.toString());
                Toast.makeText(DetailMerchantActivity.this,"Gagal memproses" +error.toString(),Toast.LENGTH_SHORT).show();
                finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<String, String>();
                param.put(VAR.ID_USER, idUser);
                param.put(VAR.ID_MERCHANT, idMerchant);
                Log.d(VAR.DEBUG,idUser+","+idMerchant);
                return param;
            }
        };
        requestQueue.add(getData);
    }

    private void setRecycler(ArrayList<MenuMerchant> menuMerchants){
        Collections.sort(menuMerchants, new MenuComparator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ListMenuAdapter(menuMerchants, this);
        recyclerView.setAdapter(mAdapter);

        recyclerViewSelected.setHasFixedSize(true);
        recyclerViewSelected.setLayoutManager(new LinearLayoutManager(this));
    }

    public class MenuComparator implements Comparator<MenuMerchant> {
        @Override
        public int compare(MenuMerchant m1, MenuMerchant m2) {
            return m1.getKategori().compareTo(m2.getKategori());
        }
    }
}
