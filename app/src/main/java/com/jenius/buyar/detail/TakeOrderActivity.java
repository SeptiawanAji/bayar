package com.jenius.buyar.detail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.jenius.buyar.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import static android.R.attr.bitmap;
import static android.R.attr.id;

public class TakeOrderActivity extends AppCompatActivity {

    private final static int QRcodeWidth = 500 ;
    private final Object lock = new Object();

    private TextView textcode, namaMerchant;
    private ImageView qrcode, qrcheck;
    private String idorder;
    private Bitmap bitmap, bitmaps;
    private ProgressDialog progressDialog;
    private CardView cvQrCode;
    private AsyncTaskRunner runner;
    private RequestQueue requestQueue;
    private StringRequest req;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_order);

        textcode = (TextView) findViewById(R.id.textcode);
        namaMerchant = (TextView) findViewById(R.id.nama_merchant);
        qrcode = (ImageView) findViewById(R.id.qrcode);
        qrcheck = (ImageView) findViewById(R.id.qr_checked);
        cvQrCode = (CardView) findViewById(R.id.cv_qrcode);

        namaMerchant.setText(getIntent().getStringExtra("namaMerchant"));
        textcode.setText(getIntent().getStringExtra("idOrder"));
        idorder = getIntent().getStringExtra("idOrder");

//        runner = new AsyncTaskRunner();
//        runner.execute();
        getQrCode();
    }

    private synchronized void getQrCode() {
        try {
            bitmap = TextToImageEncode(idorder);
            cvQrCode.setVisibility(View.VISIBLE);
            qrcode.setImageBitmap(bitmap);
            cekStatus(getIntent().getStringExtra("idOrder"));
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;

        @Override
        protected String doInBackground(String... params) {
            try {
                bitmap = TextToImageEncode(idorder);
                resp = "sukses";
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            cvQrCode.setVisibility(View.VISIBLE);
            qrcode.setImageBitmap(bitmap);
            cekStatus(getIntent().getStringExtra("idOrder"));
        }


        @Override
        protected void onPreExecute() {
            showProgres();
        }


        @Override
        protected void onProgressUpdate(String... text) {}
    }

    private void cekStatus(final String idorder) {
        if (requestQueue == null ){
            requestQueue = Volley.newRequestQueue(getApplicationContext());
            req = new StringRequest(Request.Method.POST, VAR.URLCEKSTATUS, new Response.Listener<String>(){
                @Override
                public void onResponse(String response) {
                    String status = "";
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d(VAR.DEBUG,"cek status response : "+response);
                        if (jsonObject.getString("status").equals("success")){
                            status = jsonObject.getString("status_order");
                            if (status.equals("2")){
                                qrcheck.setVisibility(View.VISIBLE);
                                new Handler().postDelayed(new Runnable(){
                                    @Override
                                    public void run() {
                                        finish();
                                        bitmap.recycle();
                                        bitmap = null;
                                        bitmaps.recycle();
                                        bitmaps = null;
                                        Runtime.getRuntime().gc();
                                    }
                                }, 2000);
                            } else if (status.equals("1")){
                                requestQueue = null;
                                cekStatus(getIntent().getStringExtra("idOrder"));
                            }
                        } else {
                            Log.d(VAR.DEBUG, "error cek status");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(VAR.DEBUG, "error cek status : " +e.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(VAR.DEBUG, "error cek status : " +error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> param = new HashMap<String, String>();
                    param.put("id_order", idorder);
                    Log.d(VAR.DEBUG,"json cekstatus : " +param.toString());
                    return param;
                }
            };
            requestQueue.add(req);
        }
    }

    private void showProgres() {
        progressDialog = null;// Initialize to null
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mendapatkan kode ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(Value, BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, getHints());
        } catch (IllegalArgumentException Illegalargumentexception) {
            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();
        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;
            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.black):getResources().getColor(R.color.white);
            }
        }
        bitmaps = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmaps.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmaps;
    }


    public Map<EncodeHintType,?> getHints() {
        Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hints.put(EncodeHintType.MARGIN, 2);

        return hints;
    }

    @Override
    public void onBackPressed() {
    }
}
