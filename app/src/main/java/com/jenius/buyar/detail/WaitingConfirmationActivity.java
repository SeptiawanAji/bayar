package com.jenius.buyar.detail;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jenius.buyar.R;
import com.jenius.buyar.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class WaitingConfirmationActivity extends AppCompatActivity {

    private final Object lock = new Object();
    private RequestQueue requestQueue;
    private StringRequest req;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_confirmation);

        cekStatus(getIntent().getStringExtra("idOrder"));
    }

    private void cekStatus(final String idorder) {
        if (requestQueue == null){
            requestQueue = Volley.newRequestQueue(getApplicationContext());
            req = new StringRequest(Request.Method.POST, VAR.URLCEKSTATUS, new Response.Listener<String>(){
                @Override
                public void onResponse(String response) {
                    String status = "";
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d(VAR.DEBUG,"cek status response : "+response);
                        if (jsonObject.getString("status").equals("success")){
                            status = jsonObject.getString("status_order");
                            if (status.equals("1")){
                                Intent intent = new Intent(WaitingConfirmationActivity.this, TakeOrderActivity.class);
                                intent.putExtra("namaMerchant",getIntent().getStringExtra("namaMerchant"));
                                intent.putExtra("idOrder",getIntent().getStringExtra("idOrder"));
                                finish();
                                startActivity(intent);
                            } else {
                                requestQueue = null;
                                cekStatus(getIntent().getStringExtra("idOrder"));
                            }
                        } else {
                            Log.d(VAR.DEBUG, "error cek status");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(VAR.DEBUG, "error cek status : " +e.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(VAR.DEBUG, "error cek status : " +error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> param = new HashMap<String, String>();
                    param.put("id_order", idorder);
                    Log.d(VAR.DEBUG,"json cekstatus : " +param.toString());
                    return param;
                }
            };
            requestQueue.add(req);
        }
    }

    @Override
    public void onBackPressed() {
    }
}
