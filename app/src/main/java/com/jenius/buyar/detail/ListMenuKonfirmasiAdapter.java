package com.jenius.buyar.detail;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.jenius.buyar.R;

import java.util.ArrayList;

/**
 * Created by kaddafi on 11/08/2017.
 */

public class ListMenuKonfirmasiAdapter extends RecyclerView.Adapter<ListMenuKonfirmasiAdapter.ViewHolder> {

    private Activity activity;
    private ArrayList<MenuMerchant> mDataset;

    public ListMenuKonfirmasiAdapter(ArrayList<MenuMerchant> mDataset, Activity context) {
        this.mDataset = mDataset;
        this.activity = context;
    }

    public ArrayList<MenuMerchant> getDataPesanan(){
        return mDataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView nama, subtotal, jumlah;
        EditText catatan;
        View garis;

        public ViewHolder(View itemView) {
            super(itemView);
            nama = (TextView) itemView.findViewById(R.id.nama_menu);
            subtotal = (TextView) itemView.findViewById(R.id.subtotal);
            jumlah = (TextView) itemView.findViewById(R.id.jumlah_pesan);
            catatan = (EditText) itemView.findViewById(R.id.catatan);
            garis = (View) itemView.findViewById(R.id.garis_batas);
        }
    }

    @Override
    public ListMenuKonfirmasiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_menu_konfirmasi, parent, false);
        return new ListMenuKonfirmasiAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListMenuKonfirmasiAdapter.ViewHolder holder, final int position) {
        MenuMerchant menuMerchant = mDataset.get(position);
        if (position==(getItemCount()-1)){
            holder.garis.setVisibility(View.GONE);
        }
        holder.nama.setText(menuMerchant.getNama());
        holder.jumlah.setText("X "+menuMerchant.getJumlahpesan());
        holder.subtotal.setText("Rp "+String.format("%,d", (menuMerchant.getJumlahpesan()*menuMerchant.getHarga())).replace(",","."));
        holder.catatan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                mDataset.get(position).setCatatan(editable.toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
