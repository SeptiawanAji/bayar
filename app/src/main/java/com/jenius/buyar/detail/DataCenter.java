package com.jenius.buyar.detail;

import java.util.ArrayList;

/**
 * Created by kaddafi on 11/08/2017.
 */

public class DataCenter {
    static ArrayList<MenuMerchant> menuMerchants = new ArrayList<>();
    public static ArrayList<MenuMerchant> getMenuMerchants(){
        menuMerchants.clear();
        menuMerchants.add(new MenuMerchant("1","Burger",10000,"Makanan","https://cdn.menuwithprice.com/Images/food_near_me/burgers-near-me.png"));
        menuMerchants.add(new MenuMerchant("2","Spageti",10000,"Makanan","https://cdn.menuwithprice.com/Images/food_near_me/burgers-near-me.png"));
        menuMerchants.add(new MenuMerchant("3","Lasagna",15000,"Makanan","https://cdn.menuwithprice.com/Images/food_near_me/burgers-near-me.png"));
        menuMerchants.add(new MenuMerchant("4","Cola",8000,"Minuman","https://cdn.menuwithprice.com/Images/food_near_me/burgers-near-me.png"));
        menuMerchants.add(new MenuMerchant("5","Milo",8000,"Minuman","https://cdn.menuwithprice.com/Images/food_near_me/burgers-near-me.png"));
        menuMerchants.add(new MenuMerchant("6","Es Teh",5000,"Minuman","https://cdn.menuwithprice.com/Images/food_near_me/burgers-near-me.png"));
        return menuMerchants;
    }
}
