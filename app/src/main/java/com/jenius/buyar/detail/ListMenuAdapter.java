package com.jenius.buyar.detail;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.jenius.buyar.R;

import java.util.ArrayList;

/**
 * Created by kaddafi on 11/08/2017.
 */

public class ListMenuAdapter extends RecyclerView.Adapter<ListMenuAdapter.ViewHolder> implements Filterable {

    private Activity activity;
    private ArrayList<MenuMerchant> mDataset;
    private ArrayList<MenuMerchant> mDatareal;
    private ArrayList<MenuMerchant> mDataSelected;
    private ImageLoader imageLoader;

    public ListMenuAdapter(ArrayList<MenuMerchant> mDataset, Activity context) {
        this.mDataset = mDataset;
        this.mDatareal = mDataset;
        this.activity = context;
        mDataSelected = new ArrayList<>();

        imageLoader = CustomVolleyRequest.getInstance(activity).getImageLoader();
    }

    public ArrayList<MenuMerchant> getMenuSelected(){
        return mDataSelected;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView namaMenu, hargaMenu, kategori, jumlah;
        int jumlahPesanan;
        RelativeLayout holderMenu;
        ImageView plus, minus;
        NetworkImageView fotoMenu;

        public ViewHolder(View itemView) {
            super(itemView);
            namaMenu = (TextView) itemView.findViewById(R.id.nama_menu);
            hargaMenu = (TextView) itemView.findViewById(R.id.harga_menu);
            kategori = (TextView) itemView.findViewById(R.id.kategori);
            fotoMenu = (NetworkImageView) itemView.findViewById(R.id.foto_menu);
            holderMenu = (RelativeLayout) itemView.findViewById(R.id.holder_menu);
            jumlah = (TextView) itemView.findViewById(R.id.jumlah);
            plus = (ImageView) itemView.findViewById(R.id.plus);
            minus = (ImageView) itemView.findViewById(R.id.minus);

            jumlahPesanan = 0;
            jumlah.setText(""+0);
        }
    }

    @Override
    public ListMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_menu, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListMenuAdapter.ViewHolder holder, final int position) {
        final int[] jumlahPesanan = {holder.jumlahPesanan};
        MenuMerchant menuMerchant = mDataset.get(position);
        holder.fotoMenu.setImageUrl(menuMerchant.getUrlimg(), imageLoader);
        holder.namaMenu.setText(menuMerchant.getNama());
        holder.hargaMenu.setText("Rp "+String.format("%,d", menuMerchant.getHarga()).replace(",","."));

        if(position==0){
            holder.kategori.setVisibility(View.VISIBLE);
            holder.kategori.setText(menuMerchant.getKategori());
        }else if(!menuMerchant.getKategori().equals(mDataset.get(position-1).getKategori())){
            holder.kategori.setVisibility(View.VISIBLE);
            holder.kategori.setText(menuMerchant.getKategori());
        }else {
            holder.kategori.setVisibility(View.GONE);
        }

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumlahPesanan[0] = jumlahPesanan[0] + 1;
                holder.jumlah.setText(""+jumlahPesanan[0]);
                tambahPesanan(mDataset.get(position),jumlahPesanan[0]);
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(jumlahPesanan[0]>0){
                    jumlahPesanan[0] = jumlahPesanan[0] - 1;
                    holder.jumlah.setText(""+jumlahPesanan[0]);
                    tambahPesanan(mDataset.get(position),jumlahPesanan[0]);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                ArrayList<MenuMerchant> results = new ArrayList<>();
                for (MenuMerchant m:mDatareal) {
                    if(m.getNama().toLowerCase().contains(constraint.toString().toLowerCase())){
                        results.add(m);
                    }
                }

                filterResults.values = results;
                filterResults.count = results.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mDataset = (ArrayList<MenuMerchant>) results.values;
                ((DetailMerchantActivity) activity).onResultPublished(mDataset.size(), constraint.toString());
                notifyDataSetChanged();
            }
        };
    }

    private void tambahPesanan (MenuMerchant menu, int jmlpesan){
        MenuMerchant menuMerchant = menu;
        if (mDataSelected.size()>0){
            int id = cekId(menuMerchant);
            if (id!=-1){
                mDataSelected.get(id).setJumlahpesan(jmlpesan);
            } else {
                menuMerchant.setJumlahpesan(jmlpesan);
                mDataSelected.add(menuMerchant);
            }
        } else {
            menuMerchant.setJumlahpesan(jmlpesan);
            mDataSelected.add(menuMerchant);
        }
        ((DetailMerchantActivity) activity).onItemSelected();
    }

    private int cekId(MenuMerchant menu){
        int result = -1;
        for (int i=0; i<mDataSelected.size(); i++){
            if (menu.getId().equals(mDataSelected.get(i).getId())){
                result = i;
            }
        }
        Log.d("DEBUG", "int result : " +result);
        return result;
    }
}
