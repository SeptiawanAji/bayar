package com.jenius.buyar.detail;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.google.vr.sdk.widgets.pano.VrPanoramaView;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by webster on 1/23/17.
 */

public class ImageLoaderTask extends AsyncTask<AssetManager, Void, Bitmap> {

    private static final String TAG = "ImageLoaderTask";
    private final String pathFile;
    private final WeakReference<VrPanoramaView> viewReference;
    private final VrPanoramaView.Options viewOptions;

    private static WeakReference<Bitmap> lastBitmap = new WeakReference<>(null);
    private static String lastName;

    public ImageLoaderTask(VrPanoramaView view, VrPanoramaView.Options viewOptions, String pathFile) {
        viewReference = new WeakReference<>(view);
        this.viewOptions = viewOptions;
        this.pathFile = pathFile;
    }

    @Override
    protected Bitmap doInBackground(AssetManager... params) {
        try {
            URL url = new URL(pathFile);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        final VrPanoramaView vw = viewReference.get();
        if (vw != null && bitmap != null) {
            vw.loadImageFromBitmap(bitmap, viewOptions);
        }
    }

}
