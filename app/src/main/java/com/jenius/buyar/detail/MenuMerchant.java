package com.jenius.buyar.detail;

/**
 * Created by kaddafi on 11/08/2017.
 */

public class MenuMerchant {
    private String id;
    private String nama;
    private String kategori;
    private String deskripsi;
    private String urlimg;
    private String catatan;
    private int harga;
    private int jumlahpesan;

    public MenuMerchant(){

    }

    public MenuMerchant(String id, String nama, int harga, String kategori, String urlimg){
        this.id = id;
        this.nama = nama;
        this.harga = harga;
        this.kategori = kategori;
        this.urlimg = urlimg;
    }

    public MenuMerchant(String id, String nama, int harga, String kategori, String urlimg, int jumlahpesan, String catatan){
        this.id = id;
        this.nama = nama;
        this.harga = harga;
        this.kategori = kategori;
        this.urlimg = urlimg;
        this.jumlahpesan = jumlahpesan;
        this.catatan = catatan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getUrlimg() {
        return urlimg;
    }

    public void setUrlimg(String urlimg) {
        this.urlimg = urlimg;
    }

    public int getJumlahpesan() {
        return jumlahpesan;
    }

    public void setJumlahpesan(int jumlahpesan) {
        this.jumlahpesan = jumlahpesan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }
}
