package com.jenius.buyar.scan;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jenius.buyar.R;
import com.jenius.buyar.SessionManager;
import com.jenius.buyar.arkit.PARController;
import com.jenius.buyar.arkit.PARFragment;
import com.jenius.buyar.arkit.PARPoiLabel;
import com.jenius.buyar.arkit.PARPoiLabelAdvanced;
import com.jenius.buyar.arkit.StikerLabel;
import com.jenius.buyar.detail.DetailMerchantActivity;
import com.jenius.buyar.detail.Merchant;
import com.jenius.buyar.detail.VAR;
import com.jenius.buyar.helper.AppContoller;
import com.jenius.buyar.sensorkit.PSKDeviceAttitude;
import com.jenius.buyar.sensorkit.enums.PSKDeviceOrientation;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ARFragment extends PARFragment {

    public static final String FLAG = "all";
    private static ArrayList<PARPoiLabel> labelRepo = new ArrayList<PARPoiLabel>();
    SessionManager sessionManager;

    private BoomMenuButton bmb;

    private CustomModalAturJarak aturJarakDialog;



    int def;
    int distance;
    int muncul;
    int set;
    //apa itu gfhf yousd ppapa
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.viewLayoutId = R.layout.ar_view;
//
        View view = super.onCreateView(inflater, container, savedInstanceState);

        bmb = (BoomMenuButton)view.findViewById(R.id.bmb_scan);
        sessionManager = new SessionManager(getActivity().getApplicationContext());

        //menu pada saat scan

        assert bmb != null;
        bmb.setButtonEnum(ButtonEnum.TextInsideCircle);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_2_1);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.Vertical);

        //set circle menu
        final TextInsideCircleButton.Builder aturStiker = new TextInsideCircleButton.Builder()
                .normalImageRes(R.drawable.ic_jaraks)
                .imagePadding(new Rect(30,30,30,30))
                .normalText("Atur Jarak")
                .normalTextColor(Color.WHITE)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        aturJarak();
                    }
                });
        final TextInsideCircleButton.Builder aturJarak = new TextInsideCircleButton.Builder()
                .normalImageRes(R.drawable.ic_muat_ulang)
                .imagePadding(new Rect(30,30,30,30))
                .normalText("Muat Ulang")
                .normalTextColor(Color.WHITE)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        getData(500);
                    }
                });

        bmb.addBuilder(aturStiker);
        bmb.addBuilder(aturJarak);
        getRadarView().setRadarRange(500);
        getData(500);

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    //==============================================================================================
    // Callback
    //==============================================================================================
    @Override
    public void onDeviceOrientationChanged(PSKDeviceOrientation newOrientation) {
        super.onDeviceOrientationChanged(newOrientation);
//        Toast.makeText(getActivity(), "onDeviceOrientationChanged: " + PSKDeviceAttitude.rotationToString(newOrientation), Toast.LENGTH_LONG).show();
    }


    public StikerLabel createStiker(final Merchant merchant){
        StikerLabel stiker = null;
        stiker = new StikerLabel(merchant,R.layout.ar_stiker,R.drawable.ic_dot_green,getActivity());

        stiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DetailMerchantActivity.class);
                intent.putExtra("idUser","1");
                Log.d("wulan_",""+merchant.getIdMerchant());
                Log.d("wulan__",""+merchant.getNamaMerchant());
                Log.d("wulan___",""+merchant.getPathFoto());
                intent.putExtra("idMerchant",""+merchant.getIdMerchant());
                intent.putExtra("namaMerchant",merchant.getNamaMerchant());
                intent.putExtra("urlImgMerchant",merchant.getPathFoto());
                intent.putExtra("urlPanorama",merchant.getPanorama());
                startActivity(intent);
            }
        });
        return stiker;
    }

    public PARPoiLabelAdvanced createPoi(String title, String description, double lat, double lon, double alt) {
        Location poiLocation = new Location(title);
        poiLocation.setLatitude(lat);
        poiLocation.setLongitude(lon);
        poiLocation.setAltitude(alt);

        final PARPoiLabelAdvanced parPoiLabel = new PARPoiLabelAdvanced(poiLocation, title, description, R.layout.ar_stiker, R.drawable.radar_dot);
        parPoiLabel.setIsAltitudeEnabled(true);
        parPoiLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), parPoiLabel.getTitle() + " - " + parPoiLabel.getDescription(), Toast.LENGTH_LONG).show();
            }
        });

        return parPoiLabel;
    }

//    private PARPoiLabelAdvanced createRepoPoi(
//            String title,
//            String description,
//            double latitude,
//            double longitude,
//            double altitude) {
//        return createPoi(title, description, latitude, longitude,altitude);
//    }
    /**
     * adds 4 points in cardinal direction at current location
     */
//    private void createCDPOIs(){
//
//        double degreeCorrection = 0.1;
//        Location currentLocation = PSKDeviceAttitude.sharedDeviceAttitude().getLocation();
//
//        PARController.getInstance().addPoi(createPoi("Utara", "", currentLocation.getLatitude()+degreeCorrection, currentLocation.getLongitude(),0.0));
//        PARController.getInstance().addPoi(createPoi("Selatan", "", currentLocation.getLatitude()-degreeCorrection, currentLocation.getLongitude(),0.0));
//        PARController.getInstance().addPoi(createPoi("Barat", "",  currentLocation.getLatitude(),                  currentLocation.getLongitude()-degreeCorrection,0.0));
//        PARController.getInstance().addPoi(createPoi("Timur", "",  currentLocation.getLatitude(),                  currentLocation.getLongitude()+degreeCorrection,0.0));
//    }


    public void setAr(final ArrayList<Merchant> merchantArrayList,int jarak){
        PARController.getInstance().clearObjects();
        ArrayList<Merchant> fix = new ArrayList<>();
        //device location
        PSKDeviceAttitude deviceAttitude = PSKDeviceAttitude.sharedDeviceAttitude();
        if (deviceAttitude == null) {
           return;
        }
        Location userLocation = deviceAttitude.getLocation();
        if (userLocation == null) {
            return;
        }else{
            for(int i=0;i<merchantArrayList.size();i++){
                if(userLocation.distanceTo(merchantArrayList.get(i).getLocation())<=jarak){
                    fix.add(merchantArrayList.get(i));
                    PARController.getInstance().addPoi( createStiker(fix.get(i)));
                }

            }
        }
    }

    public void getData(final int jarak){
        final ArrayList<Merchant> merchants = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VAR.URLDETAIL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("wulan__",response);
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("all_merchant");
                    for(int i=0;i<jsonArray.length();i++){
                        Merchant merchant = new Merchant();
                        Location location = new Location("");

                        JSONObject js = jsonArray.getJSONObject(i);
                        location.setLatitude(js.getDouble("latitude"));
                        location.setLongitude(js.getDouble("longitude"));

                        merchant.setIdMerchant(js.getInt("id_merchant"));
                        merchant.setNamaMerchant(js.getString("nama"));
                        merchant.setLocation(location);
                        merchant.setPathFoto(js.getString("path_foto"));
                        merchant.setPanorama(js.getString("panorama"));
                        merchants.add(merchant);
                    }

                    setAr(merchants,jarak);
                    Log.d("wulan",merchants.toString());

                }catch (Exception e){
                    Log.d("error__",e.toString());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppContoller.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void aturJarak(){
        aturJarakDialog = new CustomModalAturJarak(getActivity());
        aturJarakDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        aturJarakDialog.show();
        aturJarakDialog.setDialog(new CustomModalAturJarak.OnMyDialogAturJarakResult() {
            @Override
            public void finish(int jarak) {
                sessionManager.setJarak(jarak);
                getData(jarak);
            }
        });
    }
}