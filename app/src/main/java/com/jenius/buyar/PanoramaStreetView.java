package com.jenius.buyar;

import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.jenius.buyar.detail.ImageLoaderTask;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Septiawan Aji Pradan on 6/25/2017.
 */

public class PanoramaStreetView extends AppCompatActivity {
    private VrPanoramaView panoWidgetView;

    private ImageLoaderTask backgroundImageLoaderTask;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.panorama);
        panoWidgetView = (VrPanoramaView)findViewById(R.id.pano_view);
        loadPanoImage(getIntent().getStringExtra("panorama"));
    }

    @Override
    protected void onPause() {
        panoWidgetView.pauseRendering();
        super.onPause();
    }

    @Override
    protected void onResume() {
        panoWidgetView.resumeRendering();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        panoWidgetView.shutdown();
        super.onDestroy();
    }


    private synchronized void loadPanoImage(String pathFoto) {


        ImageLoaderTask task = backgroundImageLoaderTask;
        if (task != null && !task.isCancelled()) {
            // Cancel any task from a previous loading.
            task.cancel(true);
        }
        VrPanoramaView.Options viewOptions = new VrPanoramaView.Options();
        viewOptions.inputType = VrPanoramaView.Options.TYPE_MONO;


        // use the name of the image in the assets/ directory.
//
//        String pathFoto = getNearestDistance(locationBangunan);
        Log.d("_diambil",pathFoto);

        // create the task passing the widget view and call execute to start.
        task = new ImageLoaderTask(panoWidgetView, viewOptions, pathFoto);
        task.execute(this.getAssets());
        backgroundImageLoaderTask = task;
    }
}
