package com.jenius.buyar.arkit;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.location.Location;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.jenius.buyar.R;
import com.jenius.buyar.detail.CustomVolleyRequest;
import com.jenius.buyar.detail.Merchant;
import com.jenius.buyar.objek.TempatMakan;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Septiawan Aji Pradan on 5/21/2017.
 */

public class StikerLabel extends PARPoi {
    private TextView namaTempatMakan;
    private TextView harga;
    private TextView jarakT;
    private Merchant merchant;
    private NetworkImageView fotoMakanan;
    private ImageLoader imageLoader;
    private int layoutId;
    protected static Point defaultSize = new Point(220, 85);
    private View.OnClickListener onClickListener;
    protected Point size = null;
    protected boolean hasCreatedView;
    protected float _lastUpdateAtDistance;
    protected boolean isAltitudeEnabled = false;
    protected static final DecimalFormat FORMATTER_DISTANCE_LARGEST = new DecimalFormat("#### km");
    protected static final DecimalFormat FORMATTER_DISTANCE_LARGE = new DecimalFormat("###,## km");
    protected static final DecimalFormat FORMATTER_DISTANCE_SMALL = new DecimalFormat("### m");
    private float jarak;
    private String flag;
    private RelativeLayout rl_atas;

    public StikerLabel(){

    }

    public StikerLabel(Merchant merchant, int layoutId, int radarResourceId, Activity activity){
        super(merchant.getLocation());
        this.merchant= merchant;
        this.layoutId = layoutId;
        this.radarResourceId = radarResourceId;
        this.offset.set(0,0);
        this.jarak = jarak;

        imageLoader = CustomVolleyRequest.getInstance(activity).getImageLoader();
    }

    public StikerLabel(Location location){
        super(location);
    }

    public static Point getDefaultSize() {
        return defaultSize;
    }

    public static void setDefaultSize(Point defaultSize) {
        PARPoiLabel.defaultSize = defaultSize;
    }

    @Override
    protected Point getOffset() {
        return this.getOffset();
    }

    public void setOffset(Point leftTop) {
        this.offset = leftTop;
    }

    @Override
    public void createView() {



        if(this.ctx == null){
            return;
        }

        LayoutInflater inflater = (LayoutInflater)this.ctx.getSystemService("layout_inflater");
        if(inflater == null){
            return;
        }

        this._labelView = (RelativeLayout)inflater.inflate(this.layoutId,null);
        if (this.onClickListener != null) {
            this._labelView.setOnClickListener(this.onClickListener);
        }

        if (this.size == null) {
            this.size = new Point(StikerLabel.defaultSize.x, StikerLabel.defaultSize.y);
        }

        Resources r = this._labelView.getResources();
        int width = (int) TypedValue.applyDimension((int)1, (float)this.size.x, (DisplayMetrics)r.getDisplayMetrics());
        int height = (int)TypedValue.applyDimension((int)1, (float)this.size.y, (DisplayMetrics)r.getDisplayMetrics());
        this._labelView.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        this.halfSizeOfView = new PointF((float)(width / 2), (float)(height / 2));
        if (this._backgroundImageResource > -1) {
            this._labelView.setBackgroundResource(this._backgroundImageResource);
        }



        this.namaTempatMakan= (TextView)this._labelView.findViewById(R.id.nama_makanan_tv);
        this.harga= (TextView)this._labelView.findViewById(R.id.harga_makanan_tv);
        this.fotoMakanan = (NetworkImageView)this._labelView.findViewById(R.id.foto_makanan);

        this.jarakT = (TextView)this._labelView.findViewById(R.id.jarak);

        this.fotoMakanan.setImageUrl(merchant.getPathFoto(), imageLoader);
        this.namaTempatMakan.setText(merchant.getNamaMerchant());
        this.jarakT.setText(""+_lastUpdateAtDistance);

        this.hasCreatedView = true;
        this.updateContent();
    }

    @Override
    public void updateContent() {
        if (!this.hasCreatedView) {
            return;
        }
        double distance = this.distanceToUser;

            if (distance >= 10000.0) {
                if (Math.abs(distance - (double)this._lastUpdateAtDistance) < 1000.0) {
                    return;
                }
                distance = Math.floor(distance / 1000.0);

            } else if (distance > 1000.0) {
                if (Math.abs(distance - (double)this._lastUpdateAtDistance) < 100.0) {
                    return;
                }
                distance = Math.floor(distance / 1000.0);

            } else {
                if (Math.abs(distance - (double)this._lastUpdateAtDistance) < 10.0) {
                    return;
                }
                distance = Math.floor(distance / 5.0) * 5.0;

            }
            if (this.jarakT != null) {
                this.jarakT.setText(""+distance);
            }
        this._lastUpdateAtDistance = (float)this.distanceToUser;
    }

    public Point getSize() {
        return this.size;
    }

    public void setSize(Point size) {
        this.size = size;
    }

    public void setSize(int w, int h) {
        this.size = new Point(w, h);
    }

    public View.OnClickListener getOnClickListener() {
        return this.onClickListener;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        if (this._labelView != null) {
            this._labelView.setOnClickListener(onClickListener);
        }
    }
}
