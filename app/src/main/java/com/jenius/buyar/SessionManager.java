package com.jenius.buyar;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Septiawan Aji Pradan on 6/4/2017.
 */

public class SessionManager {
    Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String WELCOME = "welcome";
    public static final String JARAK = "jarak";
    public static final String JARAK_SCAN = "jarak_scan";
    public static final String BS = "bs";
    public static final String URL = "url";

    public static final String FORM_ID = "form_id";
    public static final String PATH_FORM ="path_form";

    public SessionManager(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences("session",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setFirst(String str){
        editor.putString(WELCOME,str);
        editor.commit();
    }

    public String getFirst(){
        String status = sharedPreferences.getString(WELCOME,null);
        return status;
    }

    public void setJarak(int jarak){
        editor.putInt(JARAK,jarak);
        editor.commit();
    }

    public int getJarak(){
        int jarak = sharedPreferences.getInt(JARAK,0);
        return jarak;
    }

    public void setBs(String bs){
        editor.putString(BS,bs);
        editor.commit();
    }

    public String getBS(){
        String bs = sharedPreferences.getString(BS,"002B");
        return bs;
    }

    public void setUrl(String url){
        editor.putString(URL,url);
        editor.commit();
    }

    public String getUrl(){
        String url = sharedPreferences.getString("URL", "");
        return url;
    }

    public void setFormId(String formId){
        editor.putString(FORM_ID,formId);
        editor.commit();
    }

    public String getFormId(){
        String formId = sharedPreferences.getString(FORM_ID,"");
        return formId;
    }

    public void setPathForm(String pathForm){
        editor.putString(PATH_FORM,pathForm);
        editor.commit();
    }

    public String getPathForm(){
        String pathForm = sharedPreferences.getString(PATH_FORM,"");
        return pathForm;
    }

    public void setJarakScan(int jarak){
        editor.putInt(JARAK_SCAN,jarak);
        editor.commit();
    }

    public int getJarakScan(){
        int jarak = sharedPreferences.getInt(JARAK_SCAN,200);
        return jarak;
    }

}
