/*
 * Decompiled with CFR 0_118.
 */
package com.jenius.buyar.sensorkit.enums;

public enum PSKDeviceOrientation {
    Normal,
    Left,
    UpsideDown,
    Right,
    FaceUp,
    FaceDown,
    Unknown;
    

    private PSKDeviceOrientation() {
    }
}

