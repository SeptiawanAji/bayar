/*
 * Decompiled with CFR 0_118.
 */
package com.jenius.buyar.sensorkit.enums;

public enum PSKSensorManagerStatus {
    PSKSensorManagerUnavailable,
    PSKSensorManagerIdle,
    PSKSensorManagerRestricted,
    PSKSensorManagerDenied,
    PSKSensorManagerRunning;
    

    private PSKSensorManagerStatus() {
    }
}

