/*
 * Decompiled with CFR 0_118.
 */
package com.jenius.buyar.sensorkit.enums;

public enum PSKGPSAvailabilityStatus {
    kPSKGPSAvailabilityStatusUnknown,
    kPSKGPSAvailabilityStatusAvailable,
    kPSKGPSAvailabilityStatusUnavailable;
    

    private PSKGPSAvailabilityStatus() {
    }
}

