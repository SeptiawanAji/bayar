package com.jenius.buyar.objek;

import android.location.Location;

/**
 * Created by aji on 8/10/2017.
 */

public class TempatMakan {
    private int idTempatMakan;
    private String nama;
    private Location location;
    private String alamat;
    private String pathFoto;
    private double saldo;

    public TempatMakan(String nama,Location location,String pathFoto){
        this.nama =  nama;
        this.location = location;
        this.pathFoto = pathFoto;
    }

    public int getIdTempatMakan() {
        return idTempatMakan;
    }

    public void setIdTempatMakan(int idTempatMakan) {
        this.idTempatMakan = idTempatMakan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPathFoto() {
        return pathFoto;
    }

    public void setPathFoto(String pathFoto) {
        this.pathFoto = pathFoto;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
